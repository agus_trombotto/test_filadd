<?php

use Illuminate\Database\Seeder;

class SubjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('subjects')->insert([
    		'name' => "Derecho Romano",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Introduccion al Derecho",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Problemas del Conocimiento",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Derecho Constitucional",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Privado I (General)",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Penal I",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Teoria Gral. del Proceso",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Privado II (Obligaciones)",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Penal II",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Publico, Provincial y Municipal",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Privado III (Contratos)",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Procesal Penal",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Taller I",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Administrativo",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Privado IV (Sociedades)",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Procesal Civil y Comercial",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Procesal Constitucional",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Privado V (Reales)",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Procesal Administrativo",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Laboral",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Taller II",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Politico",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Privado VI (Familia)",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Sociologia",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Practica I",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Privado VII (Daños)",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Privado VIII (Bancario)",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Filosofia",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Economia",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Concursal (Quiebras)",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Historia",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Teorias del Conflicto",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Internacional Publico",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Etica",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Practica II",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Internacional Privado",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Tributario",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Navegacion",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Recursos Naturales (Agrario)",

    		]);
    	DB::table('subjects')->insert([
    		'name' => "Practica III",

    		]);
    }

}
