<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
        	'id' => "10",
    		'category' => "Resumen",

    		]);
        DB::table('categories')->insert([
        	'id' => "11",
    		'category' => "Carpeta",

    		]);
        DB::table('categories')->insert([
        	'id' => "12",
    		'category' => "Apunte",

    		]);
        DB::table('categories')->insert([
        	'id' => "13",
    		'category' => "Examen Parcial",

    		]);
        DB::table('categories')->insert([
        	'id' => "14",
    		'category' => "Examen Final",

    		]);
        DB::table('categories')->insert([
        	'id' => "15",
    		'category' => "Filminas",

    		]);
        DB::table('categories')->insert([
        	'id' => "16",
    		'category' => "Grabaciones",

    		]);
    }
}
