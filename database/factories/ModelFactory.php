<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Post::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->name,
        'description' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'category_id' => NULL,
        'user_id' => NULL,

        'summary' => $faker->text($maxNbChars = 200) ,
        'stars' => $faker-> numberBetween($min = 1, $max = 5),
        'views' => $faker-> numberBetween($min = 1, $max = 200),
    ];
});
