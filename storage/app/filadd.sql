-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-09-2015 a las 16:41:16
-- Versión del servidor: 5.6.25
-- Versión de PHP: 5.5.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `filadd`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `id` int(11) NOT NULL,
  `file_name` varchar(120) NOT NULL,
  `carrera` varchar(50) NOT NULL,
  `materia` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `files`
--

INSERT INTO `files` (`id`, `file_name`, `carrera`, `materia`) VALUES
(1, 'Captura.PNG', '', ''),
(2, 'informe_SimulacionPSK.docx', '', ''),
(3, 'Lab03_B_Modem_FSK.pdf', 'Ingenieria en Computacion', 'Teoria de las Comunicaciones');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `ID` int(11) NOT NULL,
  `usuario` varchar(35) NOT NULL,
  `contra` varchar(32) NOT NULL,
  `nombre` varchar(35) NOT NULL,
  `apellido` varchar(35) NOT NULL,
  `mail` varchar(45) NOT NULL,
  `universidad` varchar(45) NOT NULL,
  `anioIngreso` int(4) NOT NULL,
  `carrera` varchar(35) NOT NULL,
  `date_creacion` date NOT NULL,
  `closed` varchar(3) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`ID`, `usuario`, `contra`, `nombre`, `apellido`, `mail`, `universidad`, `anioIngreso`, `carrera`, `date_creacion`, `closed`) VALUES
(1, 'gutitrombotto', '089110bfd1e06960fae6bf992db2f217', 'Agustin', 'Trombotto', 'gutitrombotto@live.com.ar', 'UNC', 2015, 'Ingenieria en Computacion', '0000-00-00', 'no'),
(2, 'francotrombotto', 'f057ef67558a58e16f49b3ce7c2b5ed6', 'Franco', 'Trombotto', 'francotrombotto@gmail.com', 'UTN', 2012, 'Ingenieria en Sistemas', '0000-00-00', 'no'),
(5, 'nicoferrer', '562833c7fd7a950d9869b3b74210bd6b', 'Nicolas', 'Ferrer', 'nicoferrer@gmail.com', 'Universidad Catolica', 2013, 'Ingenieria Industrial', '0000-00-00', 'no'),
(6, 'joacoolmedo', 'be3d52fa3a1664bab27592452e34a94a', 'Joaquin', 'Olmedo', 'joacoolmedo@gmail.com', 'Universidad Catolica', 2013, 'Ingenieria Industrial', '2015-09-29', 'no');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `mail` (`mail`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
