<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/




Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/home', 'HomeController@index');
    Route::get('/profile','UserController@profile');

    // SOCIAL AUTHENTICATION FACEBOOK
Route::get('auth/facebook', 'Auth\AuthController@FBredirectToProvider');
Route::get('auth/facebook/callback/', 'Auth\AuthController@FBhandleProviderCallback');

    // SOCIAL AUTHENTICATION GOOGLE
Route::get('auth/google', 'Auth\AuthController@GLredirectToProvider');
Route::get('auth/google/callback/', 'Auth\AuthController@GLhandleProviderCallback');


});

Route::get('/', function(){
	return view('welcome');
});
Route::get('/search', function(){
	return view('search');
});


	//RUTAS PARA FILES
Route::resource('files','FileController');
Route::get('json_search','FileController@json_search');
Route::get('files/download/{id}', 'FileController@download');

//RUTAS PARA CAMBIOS DE PERFIL



Route::get('post/getFiles/{id}', 'PostController@getFiles');

Route::post('search/results', 'SearchController@algoliaSearch');

Route::get('initAlgolia', 'SearchController@initAlgolia');