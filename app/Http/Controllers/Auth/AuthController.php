<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use Socialite;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            ]);
    }

    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return Response
     */
    public function FBredirectToProvider()
    {

        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     * Se toma le respuesta del "callback" de facebook con el usuario
     * Se fija si existe en la base de datos -> Si existe, se loguea con ese email
     *                                          -> Si no existe, se registra usuario con ese email
     * @return Response
     */
    public function FBhandleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();
        
        $userDB = User::where('email', $user->email)->first();
        if(!is_null($userDB)){
            Auth::login($userDB);
            return redirect ('home');
            

        } else{
            $new_user = new User;

            $new_user->name = $user->name;
            $new_user->email = $user->email;                       
            $new_user->provider = 'facebook';
            $new_user->save();

            Auth::login($new_user);
            return redirect ('home');
                            
        }
        
    }



    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return Response
     */

    public function GLredirectToProvider()
    {

        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     * Se toma le respuesta del "callback" de facebook con el usuario
     * Se fija si existe en la base de datos -> Si existe, se loguea con ese email
     *                                          -> Si no existe, se registra usuario con ese email
     * @return Response
     */
    public function GLhandleProviderCallback()
    {
        $user = Socialite::driver('google')->user();
        
        $userDB = User::where('email', $user->email)->first();
        if(!is_null($userDB)){
            Auth::login($userDB);
            return redirect ('home');
            

        } else{
            $new_user = new User;

            $new_user->name = $user->name;
            $new_user->email = $user->email;                       
            $new_user->provider = 'google';

            $new_user->save();

            Auth::login($new_user);
            return redirect ('home');
                            
        }
        
    }
}
