<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Http\Requests;

class PostController extends Controller
{
    public function getFiles($id){
    	$files = Post::find($id)->files;
    	return view('files.list', compact('files'));
    	/*foreach ($files as $file) {
    		$path = $file->path;
    		return $path;
    	}*/
    	//return response()->json($files);

    }
}
