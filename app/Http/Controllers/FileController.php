<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\File;
use Session;
use Redirect;
use Storage;
use Response;
use DB;
use App\Post;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $archivos = File::paginate(5);
        $posts = Post::paginate(5);
        return view('files.index', compact('archivos', 'posts'));
    }

    public function json_search(){
        $files = File::all();
        return response()->json($files);

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $subjects = DB::select('select * from subjects');
        $categories = DB::select('select * from categories');
        return view('files.create', compact('subjects', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       /* $this->validate($request, [
        'name' => 'bail|required|max:255|min:3',
        'descripcion' => 'required',
        ]);*/


        //File::create($request->all());
$post_name=$request->Input('name');
$post_description=$request->Input('descripcion');
$post_type=$request->Input('type');


$new_post = new Post;
$new_post->title = $post_name;
$new_post->description = $post_description;
$new_post->category_id = $post_type;
$new_post->save();


$files=$request->file('file');


foreach ($files as $file){
   $filename=$file->getClientOriginalName();
   $file_folder=storage_path('app');
   $subject = $request->subjects;
   $mime = $file->getClientMimeType();
   $extension = $file->getClientOriginalExtension();  //Ex: .docx
   $size=filesize($file); // size en bytes!
   $success=$file->move($file_folder,$filename);

             if ($success) {

                $new_file= new File;
                //$new_file->name=$request->Input('name');
                $new_file->path=$filename;
                //$new_file->descripcion=$request->Input('descripcion');
                $new_file->subject_id = $subject;
                $new_file->extension=$extension;
                $new_file->mimetype = $mime;
                $new_file->size = $size;
                //$new_file->save();
                $new_post->files()->save($new_file);
            }
        }


            Session::flash('message', "Archivo Subido Correctamente");
            return Redirect::to('files');




            
        /*$filename=$file->getClientOriginalName();
        $file_folder=storage_path('app');
        $subject = $request->subjects;
        $mime = $file->getClientMimeType(); //Ex application/vnd.openxmlformats-officedocument.wordprocessingml.document
        $extension = $file->getClientOriginalExtension();  //Ex: .docx
        $size=filesize($file); // size en bytes!
        //$success=Storage::disk('local')->put($filename, $file);
        $success=$file->move($file_folder,$filename);
        */

        

        
        //Storage::put($request->file('path'), 'Contents');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function download ($id)
    {

        $archivo = File::find($id);
        
        $name = $archivo->name;
        $pathToFile=storage_path('app')."/".$archivo->path;
        
        return  response()->download($pathToFile);


        
    }
}
