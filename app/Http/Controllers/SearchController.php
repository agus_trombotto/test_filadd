<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Vinkla\Algolia\Facades\Algolia;

class SearchController extends Controller
{
	public function algoliaSearch(Request $request){
		$index=Algolia::connection('main')->initIndex('getstarted_actors');

		$index->setSettings(array(
			"attributesToIndex" => array("name", "rating", "image_path"),
			"customRanking" => array("desc(name)", "asc(rating)")
			));
		//$client = new AlgoliaSearch\Client('VW4RB03LQ5', 'f7e0f5c8e989ea43be9784832a343842');
		//$index = $client->initIndex('getstarted_actors');
		$results = $index->search($request->input('query'));
		return $results;

/*$answer = $index->search('jim');
$answer = $index->search('jim', array("hitsPerPage" => 5));
$answer = $index->search('jim', array("hitsPerPage" => 5, "facets" => "*")); */
// answer object contains a "hits" attribute that contains all results
// each result contains your attributes and a _highlightResult attribute that contains highlighted version of your attributes
}


}
