<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //

     public function files()
    {
        return $this->hasMany('App\File','post_id');
    }
}
