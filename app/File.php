<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
     protected $table = "files";

     protected $fillable = ['name', 'path','descripcion'];

     public function post()
    {
        return $this->belongsTo('App\Post');
    }

    
}
