<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Contracts\Search;
use AlgoliaSearch\Client;
use App\Services\AlgoliaSearch;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Search::class, function(){
            return new AlgoliaSearch(AlgoliaSearch\Client('VW4RB03LQ5', 'f7e0f5c8e989ea43be9784832a343842'));
                
        });
    }
}

//env('ALGOLIA_APP_ID'), env('ALGOLIA_SEARCH_ID')