@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-6">
			
			@include('alerts.success')
			<table class="table">
				<thead>
					<th>Nombre</th>
					<th>Descripcion</th>
					<th>Operaciones</th>
				</thead>
				@foreach($files as $file)
				<tbody>
					<td>{{$file->path}}</td>
					<td>{{$file->extension}}</td>

					<td><a href="files/download/{{$file->id}}" class="btn btn-primary"> Get Files!</a></td>
					
					

				</tbody>
				@endforeach
			</table>
			
		</div>
	</div>
</div>


@endsection