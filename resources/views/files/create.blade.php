@extends('layouts.app')
@section('head')
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/dropzone.css" rel='stylesheet' type='text/css'>

@endsection
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-6">
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			
			@include('alerts.success')
			{!!Form::open(['route'=>'files.store', 'method'=>'POST','files' => true, 'class'=> 'form-horizontal'])!!}
			<!--<form class="form-horizontal" role="form" method="POST" action="{{ url('/files') }}" enctype="multipart/form-data">-->
			

			<div class="form-group">
				<label class="col-md-4 control-label"> Upload your file... </label>
				<div class="col-md-6">
					<!--{!!Form::file('path')!!}-->
					<input type="file" name="file[]" multiple>
				</div> 
			</div>
			
			<div class="form-group">
				<label class="col-md-4 control-label"> File Name </label>
				<div class="col-md-6">
					<input type="text" class="form-control" name="name" placeholder="File Name!">
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-4 control-label"> Description </label>
				<div class="col-md-6">
					<input type="text" class="form-control" name="descripcion" placeholder="Short Description!">
					<span id="helpBlock3" class="help-block">Description is optional!</span>
				</div>

			</div>
			<div class="form-group">
				@foreach($categories as $category)
				<input type="radio" name="type" value="{{$category->id}}"> {{$category->category}}<br>
				@endforeach
				
			</div>
			<div class="form-group">
				<select name="subjects" id="subjects">
					@foreach($subjects as $subject)
					<option value="{{$subject->id}}">{{$subject->name}}</option>
					@endforeach
				</select>
			</div>
			<div class="text-center col-md-12">
				<input type="submit" value="Upload Your File! " class="btn btn-default">
				
			</div>
			<!--</form>-->
			{!!Form::close()!!}
		</div>
	</div>
</div>
@endsection

