@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-6">
			
			@include('alerts.success')
			<table class="table">
				<thead>
					<th>Nombre</th>
					<th>Descripcion</th>
					<th>Operaciones</th>
				</thead>
				@foreach($posts as $post)
				<tbody>
					<td>{{$post->title}}</td>
					<td>{{$post->description}}</td>

					<td><a href="post/getFiles/{{$post->id}}" class="btn btn-primary"> Get Files!</a></td>
					
					

				</tbody>
				@endforeach
			</table>
			{!!$posts->render()!!}
		</div>
	</div>
</div>


@endsection