@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">User Profile</div>

                <div class="panel-body">
                    <div class="row">
                        <div class="text-left"><h2>Hola <b> {{ Auth::user()->name }} </b>  </h2></div>
                        <div class="text-center"> <a href="{{ url('user/editName') }}" class="btn btn-default">Edit</a></div>
                    </div>
                    
                    
                    <h4>{{Auth::user()->bio}}</h4>
                    <p>{{ Auth::user()->email}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

