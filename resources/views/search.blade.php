<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>

    <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>

    
    
    <script>

    </script>
    <style>
    html, body {
        height: 100%;
    }

    body {
        margin: 0;
        padding: 0;
        width: 100%;
        display: table;
        font-weight: 100;
        font-family: 'Lato';
    }

    .container {
        text-align: center;
        display: table-cell;
        vertical-align: middle;
    }

    .content {
        text-align: center;
        display: inline-block;
    }

    .title {
        font-size: 96px;
    }
    </style>
</head>
<body>
    <div class="container">
        <h2> Search Results</h2>
        {!!Form::open(['url'=>'search/results', 'method'=>'POST','class'=> 'form-horizontal'])!!}
        <div class="form-group">
            <label class="control-label"> Search Your File</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="query" placeholder="Busca aqui!">
            </div>
        </div>
        <div class="text-center col-md-12">
            <input type="submit" value="Search" class="btn btn-default">

        </div>
        {!!Form::close()!!}
    </div>


</body>
</html>
